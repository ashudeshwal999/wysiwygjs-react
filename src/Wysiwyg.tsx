import React, { useRef, useState } from 'react'
import { cloneDeep } from 'lodash'
import { useWysiwygEditor } from './useWysiwygEditor'
import { Toolbar } from './elements/Toolbar'
import { WysiwygJsEditor } from './wysiwygjs/WysiwygJsEditor'
import { ReferencesByCategory } from './types/References'

export type WysiwygPopoverType =
  | 'fontcolor'
  | 'highlightcolor'
  | 'linkform'
  | 'templates'
  | 'references'

export interface WysiwygTemplate {
  name: string
  template: string
}

interface Props {
  onChange?: (content: string) => void
  templates?: WysiwygTemplate[]
  references?: ReferencesByCategory
  initialValue?: string
  value?: string
  placeholder?: string
  disabled?: boolean
  username?: string
  onPasteImage?: (file: File) => Promise<string>
}

export function Wysiwyg({
  onChange,
  templates = [],
  references = [],
  initialValue = '',
  value,
  placeholder = '',
  disabled = false,
  username,
  onPasteImage,
}: Props) {
  const [wysiwygEditor, setWysiwygEditor] = useState<WysiwygJsEditor | null>(null)
  const containerRef = useRef(null)

  useWysiwygEditor({
    containerRef,
    username,
    onEditorObjectChange: (editor) => setWysiwygEditor(cloneDeep(editor)),
    onContentChange: onChange,
    initialValue,
    value,
    disabled,
    onPasteImage,
  })

  return (
    <div className="wysiwygcontainer flex flex-col w-full" ref={containerRef}>
      <div className="flex w-full">
          <span className="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500">
            {placeholder}
          </span>
          <div className="wysiwygeditor text-sm flex-1 block w-full focus:ring-indigo-500 focus:border-indigo-500 min-w-0 rounded-none rounded-r-md border-gray-300 p-2" />
      </div>
      <div className="wysiwygpreview" />
      <Toolbar
        wysiwygEditor={wysiwygEditor}
        templates={templates}
        references={references}
      />
    </div>
  )
}

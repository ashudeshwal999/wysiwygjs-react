import React from 'react'
import { HighlightColorPopover } from './popovers/HighlightColorPopover'
import { FontColorPopover } from './popovers/FontColorPopover'
import { WysiwygJsEditor } from '../wysiwygjs/WysiwygJsEditor'
import { ReferencesPopover } from './popovers/ReferencesPopover'
import { WysiwygTemplate } from '../Wysiwyg'
import { LinkFormPopover } from './popovers/LinkFormPopover'
import { TemplatesPopover } from './popovers/TemplatesPopover'
import { ReferencesByCategory } from '../types/References'

interface Props {
  wysiwygEditor: WysiwygJsEditor | null
  templates: WysiwygTemplate[]
  references: ReferencesByCategory
}

export function Popovers({ wysiwygEditor, templates, references }: Props) {
  if (
    !wysiwygEditor ||
    !wysiwygEditor.currentlyOpenPopover ||
    !wysiwygEditor.popoverAnchorElement
  ) {
    return null
  }

  let popoverComponent
  switch (wysiwygEditor.currentlyOpenPopover) {
    case 'highlightcolor':
      popoverComponent = <HighlightColorPopover wysiwygEditor={wysiwygEditor} />
      break
    case 'linkform':
      popoverComponent = <LinkFormPopover wysiwygEditor={wysiwygEditor} />
      break
    case 'templates':
      popoverComponent = (
        <TemplatesPopover wysiwygEditor={wysiwygEditor} templates={templates} />
      )
      break
    case 'references':
      popoverComponent = (
        <ReferencesPopover references={references} wysiwygEditor={wysiwygEditor} />
      )
      break
    case 'fontcolor':
      popoverComponent = <FontColorPopover wysiwygEditor={wysiwygEditor} />
      break
    default:
      throw new Error(
        'Popover type ' + wysiwygEditor.currentlyOpenPopover + ' not implemented',
      )
  }

  return (
    <div
      style={{
        position: 'absolute',
        left: wysiwygEditor.popoverAnchorElement.offsetLeft,
        top: wysiwygEditor.popoverAnchorElement.offsetHeight + 10,
      }}
      key={wysiwygEditor.currentlyOpenPopover ?? ''} // Changing the key of a component will force a rerender of that component. We want that if the popoverComponent is changed
    >
      {popoverComponent}
    </div>
  )
}

import React from 'react'
import { WysiwygJsEditor } from '../wysiwygjs/WysiwygJsEditor'
import { WysiwygTemplate } from '../Wysiwyg'
import { Popovers } from './Popovers'
import { ReferencesByCategory } from '../types/References'

interface Props {
  wysiwygEditor: WysiwygJsEditor | null
  templates: WysiwygTemplate[]
  references: ReferencesByCategory
}

export function Toolbar({ wysiwygEditor, templates, references }: Props) {
  return (
    <div className="wysiwygbuttons" style={{ position: 'relative' }}>
      <button
        className="wysiwygbutton fixed-size"
        title="Toggle HTML source"
        id="wysiwyg-src"
        onClick={() => wysiwygEditor && wysiwygEditor.buttonAction('src')}
      >
        &lt;&gt;
      </button>
      <button
        className="wysiwygbutton fixed-size"
        title="Bold (Ctrl+B)"
        id="wysiwyg-bold"
        onClick={() => wysiwygEditor && wysiwygEditor.buttonAction('bold')}
      >
        B
      </button>
      <button
        className="wysiwygbutton fixed-size"
        title="Italic (Ctrl+I)"
        id="wysiwyg-italic"
        onClick={() => wysiwygEditor && wysiwygEditor.buttonAction('italic')}
      >
        I
      </button>
      <button
        className="wysiwygbutton fixed-size"
        title="Underline (Ctrl+U)"
        id="wysiwyg-underline"
        onClick={() => wysiwygEditor && wysiwygEditor.buttonAction('underline')}
      >
        U
      </button>
      <button
        className="wysiwygbutton fixed-size"
        title="Code/Monospace font"
        id="wysiwyg-monospace"
        onClick={() => wysiwygEditor && wysiwygEditor.buttonAction('monospace')}
      >
        M
      </button>
      <button
        className="wysiwygbutton fixed-size"
        title="Set text color"
        id="wysiwyg-fontcolor"
        onClick={(event) =>
          wysiwygEditor && wysiwygEditor.buttonAction('fontcolor', event.target)
        }
      >
        A<span style={{ verticalAlign: 'sub', marginLeft: '-0.0rem' }}>&#9646;</span>
      </button>
      <button
        className="wysiwygbutton fixed-size"
        title="Set highlight color"
        id="wysiwyg-highlightcolor"
        onClick={(event) =>
          wysiwygEditor && wysiwygEditor.buttonAction('highlightcolor', event.target)
        }
      >
        <svg
          viewBox="0 0 32 32"
          width="32"
          height="32"
          fill="none"
          stroke="currentcolor"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
        >
          <path d="M30 7 L25 2 5 22 3 29 10 27 Z M21 6 L26 11 Z M5 22 L10 27 Z" />
        </svg>
        <span style={{ position: 'relative', top: '-23px', left: '8px' }}>&#9646;</span>
      </button>
      <button
        className="wysiwygbutton fixed-size"
        title="Heading 1"
        id="wysiwyg-heading1"
        onClick={() => wysiwygEditor && wysiwygEditor.buttonAction('heading1')}
      >
        H1
      </button>
      <button
        className="wysiwygbutton fixed-size"
        title="Heading 2"
        id="wysiwyg-heading2"
        onClick={() => wysiwygEditor && wysiwygEditor.buttonAction('heading2')}
      >
        H2
      </button>
      <button
        className="wysiwygbutton fixed-size"
        title="Normal text"
        id="wysiwyg-paragraph"
        onClick={() => wysiwygEditor && wysiwygEditor.buttonAction('paragraph')}
      >
        P
      </button>
      <button
        className="wysiwygbutton fixed-size"
        title="Numbered list"
        id="wysiwyg-orderedList"
        onClick={() => wysiwygEditor && wysiwygEditor.buttonAction('orderedList')}
      >
        <svg
          width="32"
          height="32"
          fill="currentcolor"
          stroke="currentcolor"
          viewBox="0 0 1792 1792"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M381 1620q0 80-54.5 126t-135.5 46q-106 0-172-66l57-88q49 45 106 45 29 0 50.5-14.5t21.5-42.5q0-64-105-56l-26-56q8-10 32.5-43.5t42.5-54 37-38.5v-1q-16 0-48.5 1t-48.5 1v53h-106v-152h333v88l-95 115q51 12 81 49t30 88zm2-627v159h-362q-6-36-6-54 0-51 23.5-93t56.5-68 66-47.5 56.5-43.5 23.5-45q0-25-14.5-38.5t-39.5-13.5q-46 0-81 58l-85-59q24-51 71.5-79.5t105.5-28.5q73 0 123 41.5t50 112.5q0 50-34 91.5t-75 64.5-75.5 50.5-35.5 52.5h127v-60h105zm1409 319v192q0 13-9.5 22.5t-22.5 9.5h-1216q-13 0-22.5-9.5t-9.5-22.5v-192q0-14 9-23t23-9h1216q13 0 22.5 9.5t9.5 22.5zm-1408-899v99h-335v-99h107q0-41 .5-122t.5-121v-12h-2q-8 17-50 54l-71-76 136-127h106v404h108zm1408 387v192q0 13-9.5 22.5t-22.5 9.5h-1216q-13 0-22.5-9.5t-9.5-22.5v-192q0-14 9-23t23-9h1216q13 0 22.5 9.5t9.5 22.5zm0-512v192q0 13-9.5 22.5t-22.5 9.5h-1216q-13 0-22.5-9.5t-9.5-22.5v-192q0-13 9.5-22.5t22.5-9.5h1216q13 0 22.5 9.5t9.5 22.5z" />
        </svg>
      </button>
      <button
        className="wysiwygbutton fixed-size"
        title="Bullet list"
        id="wysiwyg-unorderedList"
        onClick={() => wysiwygEditor && wysiwygEditor.buttonAction('unorderedList')}
      >
        <svg
          width="32"
          height="32"
          fill="currentcolor"
          stroke="currentcolor"
          viewBox="0 0 1792 1792"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            className="path1"
            d="M384 1408q0 80-56 136t-136 56-136-56-56-136 56-136 136-56 136 56 56 136zm0-512q0 80-56 136t-136 56-136-56-56-136 56-136 136-56 136 56 56 136zm1408 416v192q0 13-9.5 22.5t-22.5 9.5h-1216q-13 0-22.5-9.5t-9.5-22.5v-192q0-13 9.5-22.5t22.5-9.5h1216q13 0 22.5 9.5t9.5 22.5zm-1408-928q0 80-56 136t-136 56-136-56-56-136 56-136 136-56 136 56 56 136zm1408 416v192q0 13-9.5 22.5t-22.5 9.5h-1216q-13 0-22.5-9.5t-9.5-22.5v-192q0-13 9.5-22.5t22.5-9.5h1216q13 0 22.5 9.5t9.5 22.5zm0-512v192q0 13-9.5 22.5t-22.5 9.5h-1216q-13 0-22.5-9.5t-9.5-22.5v-192q0-13 9.5-22.5t22.5-9.5h1216q13 0 22.5 9.5t9.5 22.5z"
          />
        </svg>
      </button>
      <button
        className="wysiwygbutton fixed-size"
        title="Add link"
        id="wysiwyg-link"
        onClick={(event) =>
          wysiwygEditor && wysiwygEditor.buttonAction('linkform', event.target)
        }
      >
        <svg
          width="32"
          height="32"
          id="i-link"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 32 32"
          fill="none"
          stroke="currentcolor"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="6.25%"
        >
          <path d="M18 8 C18 8 24 2 27 5 30 8 29 12 24 16 19 20 16 21 14 17 M14 24 C14 24 8 30 5 27 2 24 3 20 8 16 13 12 16 11 18 15" />
        </svg>
      </button>
      <button
        className="wysiwygbutton fixed-size"
        title="Clear formatting"
        id="wysiwyg-removeFormat"
        onClick={() => wysiwygEditor && wysiwygEditor.buttonAction('removeFormat')}
      >
        T<span style={{ verticalAlign: 'sub', marginLeft: '-0.3rem' }}>&#10005;</span>
      </button>
      <button
        className="wysiwygbutton fixed-size id-insert-template-button"
        title="Insert template"
        id="wysiwyg-templates"
        onClick={(event) =>
          wysiwygEditor && wysiwygEditor.buttonAction('templates', event.target)
        }
        disabled={!templates || !templates.length}
      >
        <svg
          viewBox="0 0 32 32"
          width="32"
          height="32"
          fill="none"
          stroke="currentcolor"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
        >
          <path d="M18 13 L26 2 8 13 14 19 6 30 24 19 Z" />
        </svg>
      </button>
      <button
        className="wysiwygbutton fixed-size id-insert-signature-button"
        title="Insert signature (Alt+S)"
        id="wysiwyg-signature"
        onClick={() => wysiwygEditor && wysiwygEditor.buttonAction('signature')}
      >
        <svg
          viewBox="0 0 32 32"
          width="32"
          height="32"
          fill="none"
          stroke="currentcolor"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
        >
          <path d="M22 11 C22 16 19 20 16 20 13 20 10 16 10 11 10 6 12 3 16 3 20 3 22 6 22 11 Z M4 30 L28 30 C28 21 22 20 16 20 10 20 4 21 4 30 Z" />
        </svg>
      </button>
      <button
        className="wysiwygbutton fixed-size id-insert-references-button"
        ng-class="{'hidden': !$ctrl.references}"
        title="Insert reference"
        id="wysiwyg-references"
        onClick={(event) =>
          wysiwygEditor && wysiwygEditor.buttonAction('references', event.target)
        }
        ng-disabled="!$ctrl.references || $ctrl.references.length == 0"
      >
        <svg
          viewBox="0 0 32 32"
          width="32"
          height="32"
          fill="none"
          stroke="currentcolor"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
        >
          <path d="M16 7 C16 7 9 1 2 6 L2 28 C9 23 16 28 16 28 16 28 23 23 30 28 L30 6 C23 1 16 7 16 7 Z M16 7 L16 28" />
        </svg>
      </button>

      <Popovers
        wysiwygEditor={wysiwygEditor}
        templates={templates}
        references={references}
      />
    </div>
  )
}

import React from 'react'
import { WysiwygJsEditor } from '../../wysiwygjs/WysiwygJsEditor'
import { WysiwygTemplate } from '../../Wysiwyg'

interface Props {
  templates: WysiwygTemplate[]
  wysiwygEditor: WysiwygJsEditor
}

export function TemplatesPopover({ wysiwygEditor, templates }: Props) {
  return (
    <div className="wysiwygtemplatespopover">
      {templates.map((template) => (
        <div key={template.name} className="template-item">
          <button
            className="template-button"
            onClick={() => wysiwygEditor.insertTemplate(template)}
          >
            +
          </button>
          <div className="template-title whitespace-nowrap">{template.name}</div>
        </div>
      ))}
    </div>
  )
}
